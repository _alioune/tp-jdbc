package tp_jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Exo2 {
	public static void listEleves() throws SQLException {
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_jdbc","jdbc_user","jdbc");
		PreparedStatement ps = 
				conn.prepareStatement("SELECT * FROM eleves WHERE age > ?");
		ps.setInt(1, 25);
		ResultSet rs=ps.executeQuery();
		 // exploitation du resultat
		 while (rs.next()) {
			 System.out.println(rs.getString("nom")) ;
		}
		 conn.close();
	}
	public static void meanAge() throws SQLException {
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_jdbc","jdbc_user","jdbc");
		Statement smt =conn.createStatement();
		String sql="SELECT avg(eleves.age), avg(professeurs.age) FROM eleves,professeurs";
		ResultSet rs=smt.executeQuery(sql);
		while(rs.next()) {
			System.out.println("moyenne_age_eleves="+rs.getDouble(1)+" moyenne_age_prof="+rs.getDouble(2));
		}
		conn.close();
	}
	public static void meanAge2() throws SQLException {
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_jdbc","jdbc_user","jdbc");
		Statement smt =conn.createStatement();
		String sql="SELECT avg(eleves.age) FROM eleves INNER JOIN professeurs ON eleves.nom=professeurs.nom";
		ResultSet rs=smt.executeQuery(sql);
		while(rs.next()) {
			System.out.println("moyenne_age_�l�ves_et_aussi_prof="+rs.getDouble(1));
		}
	}
	public static void meanPrixIns() throws SQLException {
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_jdbc","jdbc_user","jdbc");
		PreparedStatement ps = 
				conn.prepareStatement("SELECT avg(prix) FROM instruments WHERE location= ?");
		ps.setString(1, "N");
		ResultSet rs=ps.executeQuery();
		while(rs.next()) {
			System.out.println("prix moyen instruments pas louable= "+rs.getDouble(1));
		}
	}
	public static void main(String[] args) {
		try {
			listEleves();
			System.out.println("---------------------------------------");
			meanAge();
			System.out.println("---------------------------------------");
			meanAge2();
			System.out.println("---------------------------------------");
			meanPrixIns();
			System.out.println("---------------------------------------");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
