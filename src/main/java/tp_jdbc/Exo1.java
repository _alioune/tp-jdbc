package tp_jdbc;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class Exo1 {
	private static Map<String,Long> keys=new HashMap<>();
	public static void insertStudent(String path) throws IOException, SQLException{
		File file=new File(path);
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_jdbc","jdbc_user","jdbc");
		PreparedStatement ps=conn.prepareStatement("insert into eleves(nom,age) values(?,?)");
		
		try(InputStream is=new FileInputStream(file);
				BufferedInputStream bis=new BufferedInputStream(is);
				BufferedReader br = new BufferedReader(
				        new InputStreamReader(bis, StandardCharsets.UTF_8))) {
			System.out.println(br.readLine());
			
			while(br.ready()){
				String line=br.readLine();
				String[] tab=line.split("\\s+");
				System.out.println(tab.length);
				System.out.println("nom: "+tab[0]);
				System.out.println("age: "+tab[1]);
			    ps.setString(1, tab[0]);
			    ps.setInt(2, Integer.parseInt(tab[1]));
			    ps.execute();
			  
			}
			conn.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	public static void insertProf(String path) throws SQLException, IOException{
		File file=new File(path);
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_jdbc","jdbc_user","jdbc");
		PreparedStatement ps=conn.prepareStatement("insert into professeurs(nom,age,instrument) values(?,?,?)");
		try(InputStream is=new FileInputStream(file);
				BufferedInputStream bis=new BufferedInputStream(is);
				BufferedReader br = new BufferedReader(
				        new InputStreamReader(bis, StandardCharsets.UTF_8))) {
			System.out.println(br.readLine());
			while(br.ready()){
				String line=br.readLine();
				String[] tab=line.split("\\s+");
			    ps.setString(1, tab[0]);
			    ps.setInt(2, Integer.parseInt(tab[1]));
			    ps.setInt(3, 0);
			    ps.execute();
			}
			conn.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void insertInstrument(String path) throws SQLException, FileNotFoundException, IOException{
		File file=new File(path);
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_jdbc","jdbc_user","jdbc");
		PreparedStatement ps=conn.prepareStatement("insert into instruments(Nom,Prix,Prix_Cours,Location) values(?,?,?,?)");
		try(InputStream is=new FileInputStream(file);
				BufferedInputStream bis=new BufferedInputStream(is);
				BufferedReader br = new BufferedReader(
				        new InputStreamReader(bis, StandardCharsets.UTF_8))) {
			while(br.ready()){
				String line=br.readLine();
				if(line.startsWith("#"))
					continue;
				if(line.startsWith("Nom"))
					continue;
				String[] tab=line.split("\\s+");
				System.out.println("nom: "+tab[0]);
				System.out.println("instrument: "+tab[1]);
				
				if(tab.length==2){
					ps.setString(1, tab[0]);
				    ps.setInt(2, Integer.parseInt(tab[1]));
				    ps.setInt(3,0);
				    ps.setString(4, "");
				}
				else{
					ps.setString(1, tab[0]);
				    ps.setInt(2, Integer.parseInt(tab[1]));
				    ps.setInt(3, Integer.parseInt(tab[2]));
				    ps.setString(4, tab[3]);
				}
				ps.execute();
			}
			conn.close();
		}
	}
	public static void insertProfInst(String path) throws SQLException, IOException {
		File file=new File(path);
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_jdbc","jdbc_user","jdbc");
		PreparedStatement ps=conn.prepareStatement("insert into professeurs_instruments(nom,instrument) values(?,?)",
				Statement.RETURN_GENERATED_KEYS);
		try(InputStream is=new FileInputStream(file);
				BufferedInputStream bis=new BufferedInputStream(is);
				BufferedReader br = new BufferedReader(
				        new InputStreamReader(bis, StandardCharsets.UTF_8))) {
			System.out.println(br.readLine());
			
			while(br.ready()){
				String line=br.readLine();
				String[] tab=line.split("\\s+");
				String key=tab[0];
				System.out.println("nom: "+tab[0]);
				System.out.println("instrument: "+tab[1]);
			    ps.setString(1, tab[0]);
			    ps.setString(2, tab[1]);
			    ps.execute();
			    ResultSet rs = ps.getGeneratedKeys();
                long value=0L;
			    if (rs.next()) {
			        value = rs.getLong(1);
			    }
			    keys.put(key, value);
				
			}
			conn.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void insertElevesInst(String path) throws SQLException, IOException {
		File file=new File(path);
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_jdbc","jdbc_user","jdbc");
		PreparedStatement ps=conn.prepareStatement("insert into eleves_instruments(nom,instrument) values(?,?)");
		try(InputStream is=new FileInputStream(file);
				BufferedInputStream bis=new BufferedInputStream(is);
				BufferedReader br = new BufferedReader(
				        new InputStreamReader(bis, StandardCharsets.UTF_8))) {
			System.out.println(br.readLine());
			while(br.ready()){
				String line=br.readLine();
				String[] tab=line.split("\\s+");
			    for(int i=1;i<tab.length;i++) {
			    	ps.setString(1, tab[0]);
			    	ps.setString(2, tab[i]);
			    	ps.execute();
			    }  
			}
			conn.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void main(String... args) throws SQLException{
		String path="eleves.txt";
		String path2="professeurs.txt";
		String path3="instruments.txt";
		String path4="professeurs-instruments.txt";
		String path5="eleves-instruments.txt";
		try {
			insertStudent(path);
			insertProf(path2);
			insertInstrument(path3);
			insertProfInst(path4);
			keys.forEach((key, value) -> {
			    System.out.println("Key : " + key + " Value : " + value);
			});
			insertElevesInst(path5);
		} catch (IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			//update de professeurs
			Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_jdbc","jdbc_user","jdbc");
			String query = "update professeurs set instrument = ? where nom = ?";
			PreparedStatement ps=conn.prepareStatement(query);
			System.out.println(keys.size());
			
			keys.forEach((key,value)->{
				 
				System.out.println("Key : " + key + " Value : " + value);
				try {
					ps.setLong(1, value);
					ps.setString(2,key);
					ps.executeUpdate();
					
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			conn.close();
			
			
		} 

}
