package tp_jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcConnection {
	public static void main(String[] args) throws SQLException{
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/tp_jdbc","jdbc_user","jdbc");
		Statement statement=conn.createStatement();
		ResultSet rs=statement.executeQuery("select * from user");
		while(rs.next()){
			int id=rs.getInt("id");
			int age=rs.getInt("age");
			String name=rs.getString("name");
			System.out.println("id:"+id+" age: "+age+" name: "+name);
		}
		PreparedStatement ps=conn.prepareStatement("insert into user(id,name,age) values(?,?,?)");
	    ps.setInt(1, 3);
	    ps.setString(2, "Didier");
	    ps.setInt(3, 24);
		
	}

}
