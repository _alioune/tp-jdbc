CREATE TABLE `eleves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nom` (`nom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `instruments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `prix` int(11) NOT NULL,
  `prix_cours` int(11) NOT NULL,
  `location` char(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nom` (`nom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `professeurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `age` int(11) NOT NULL,
  `instrument` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nom` (`nom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE professeurs_instruments(
  id int(11) NOT NULL AUTO_INCREMENT,
  nom varchar(30) NOT NULL,
  instrument varchar(30) NOT NULL,
  PRIMARY KEY(id),
  UNIQUE KEY `nom` (`nom`),
  UNIQUE KEY instrument(instrument),
  foreign key (nom) REFERENCES professeurs(nom),
  foreign key(instrument) REFERENCES instruments(nom)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE eleves_instruments(
 id int(11) NOT NULL AUTO_INCREMENT,
 nom varchar(30) NOT NULL,
 instrument varchar(30) NOT NULL,
 PRIMARY KEY (id),
 foreign key(nom) REFERENCES eleves(nom),
 foreign key(instrument) REFERENCES instruments(nom) 
)ENGINE=InnoDB DEFAULT CHARSET=utf8;
drop table professeurs_instruments;
select * from eleves;
select * from professeurs;
select * from instruments;
select * from professeurs_instruments;
select * from eleves_instruments;
delete from instruments;
delete from eleves;
delete from professeurs;
delete from eleves_instruments;
delete from professeurs_instruments;
drop table professeurs;
drop table eleves;
drop table users;